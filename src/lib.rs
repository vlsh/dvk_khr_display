#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

#[macro_use]
extern crate bitflags;
extern crate libc;
extern crate shared_library;
#[macro_use]
extern crate dvk;
extern crate dvk_khr_surface;

use self::libc::{c_void, c_char, uint32_t, size_t, uint64_t, c_float, int32_t, uint8_t};
use self::shared_library::dynamic_library::DynamicLibrary;
use std::path::{Path};
use std::ffi::CString;
use dvk::*;
use dvk_khr_surface::*;

VK_DEFINE_NON_DISPATCHABLE_HANDLE!(VkDisplayKHR);
VK_DEFINE_NON_DISPATCHABLE_HANDLE!(VkDisplayModeKHR);

pub const VK_KHR_DISPLAY_SPEC_VERSION: uint32_t = 21;
pub const VK_KHR_DISPLAY_EXTENSION_NAME: *const c_char = b"VK_KHR_display\0" as *const u8 as *const c_char;

#[repr(i32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkKhrDisplayResult {
    VK_SUCCESS = 0,
    // VK_NOT_READY = 1,
    // VK_TIMEOUT = 2,
    // VK_EVENT_SET = 3,
    // VK_EVENT_RESET = 4,
    VK_INCOMPLETE = 5,
    VK_ERROR_OUT_OF_HOST_MEMORY = -1,
    VK_ERROR_OUT_OF_DEVICE_MEMORY = -2,
    // VK_ERROR_INITIALIZATION_FAILED = -3,
    // VK_ERROR_DEVICE_LOST = -4,
    // VK_ERROR_MEMORY_MAP_FAILED = -5,
    // VK_ERROR_LAYER_NOT_PRESENT = -6,
    // VK_ERROR_EXTENSION_NOT_PRESENT = -7,
    // VK_ERROR_FEATURE_NOT_PRESENT = -8,
    // VK_ERROR_INCOMPATIBLE_DRIVER = -9,
    // VK_ERROR_TOO_MANY_OBJECTS = -10,
    // VK_ERROR_FORMAT_NOT_SUPPORTED = -11,
    // VK_ERROR_SURFACE_LOST_KHR = -1000000000,
    // VK_ERROR_NATIVE_WINDOW_IN_USE_KHR = -1000000001,
    // VK_SUBOPTIMAL_KHR = 1000001003,
    // VK_ERROR_OUT_OF_DATE_KHR = -1000001004,
    // VK_ERROR_INCOMPATIBLE_DISPLAY_KHR = -1000003001,
    // VK_ERROR_VALIDATION_FAILED_EXT = -1000011001,
    // VK_ERROR_INVALID_SHADER_NV = -1000012000
}

#[repr(u32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkKhrDisplayStructureType {
    VK_STRUCTURE_TYPE_DISPLAY_MODE_CREATE_INFO_KHR = 1000002000,
    VK_STRUCTURE_TYPE_DISPLAY_SURFACE_CREATE_INFO_KHR = 1000002001
}

bitflags! {
    pub flags VkDisplayPlaneAlphaFlagBitsKHR: VkFlags {
        const VK_DISPLAY_PLANE_ALPHA_OPAQUE_BIT_KHR = 0x00000001,
        const VK_DISPLAY_PLANE_ALPHA_GLOBAL_BIT_KHR = 0x00000002,
        const VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_BIT_KHR = 0x00000004,
        const VK_DISPLAY_PLANE_ALPHA_PER_PIXEL_PREMULTIPLIED_BIT_KHR = 0x00000008
    }
}

pub type VkDisplayPlaneAlphaFlagsKHR = VkFlags;

pub type VkDisplayModeCreateFlagsKHR = VkFlags;
pub type VkDisplaySurfaceCreateFlagsKHR = VkFlags;

#[repr(C)]
pub struct VkDisplayPropertiesKHR {
    pub display: VkDisplayKHR,
    pub displayName: *const c_char,
    pub physicalDimensions: VkExtent2D,
    pub physicalResolution: VkExtent2D,
    pub supportedTransforms: VkSurfaceTransformFlagsKHR,
    pub planeReorderPossible: VkBool32,
    pub persistentContent: VkBool32
}

#[repr(C)]
pub struct VkDisplayModeParametersKHR {
    pub visibleRegion: VkExtent2D,
    pub refreshRate: uint32_t
}

#[repr(C)]
pub struct VkDisplayModePropertiesKHR {
    pub displayMode: VkDisplayModeKHR,
    pub parameters: VkDisplayModeParametersKHR
}

#[repr(C)]
pub struct VkDisplayModeCreateInfoKHR {
    pub sType: VkKhrDisplayStructureType,
    pub pNext: *const c_void,
    pub flags: VkDisplayModeCreateFlagsKHR,
    pub parameters: VkDisplayModeParametersKHR
}

#[repr(C)]
pub struct VkDisplayPlaneCapabilitiesKHR {
    pub supportedAlpha: VkDisplayPlaneAlphaFlagsKHR,
    pub minSrcPosition: VkOffset2D,
    pub maxSrcPosition: VkOffset2D,
    pub minSrcExtent: VkExtent2D,
    pub maxSrcExtent: VkExtent2D,
    pub minDstPosition: VkOffset2D,
    pub maxDstPosition: VkOffset2D,
    pub minDstExtent: VkExtent2D,
    pub maxDstExtent: VkExtent2D
}

#[repr(C)]
pub struct VkDisplayPlanePropertiesKHR {
    pub currentDisplay: VkDisplayKHR,
    pub currentStackIndex: uint32_t
}

#[repr(C)]
pub struct VkDisplaySurfaceCreateInfoKHR {
    pub sType: VkKhrDisplayStructureType,
    pub pNext: *const c_void,
    pub flags: VkDisplaySurfaceCreateFlagsKHR,
    pub displayMode: VkDisplayModeKHR,
    pub planeIndex: uint32_t,
    pub planeStackIndex: uint32_t,
    pub transform: VkSurfaceTransformFlagBitsKHR,
    pub globalAlpha: c_float,
    pub alphaMode: VkDisplayPlaneAlphaFlagBitsKHR,
    pub imageExtent: VkExtent2D
}

pub type vkGetPhysicalDeviceDisplayPropertiesKHRFn = unsafe extern "stdcall" fn(physicalDevice: VkPhysicalDevice, 
                                                                                pPropertyCount: *mut uint32_t,
                                                                                pProperties: *mut VkDisplayPropertiesKHR) -> VkKhrDisplayResult;

pub type vkGetPhysicalDeviceDisplayPlanePropertiesKHRFn = unsafe extern "stdcall" fn(physicalDevice: VkPhysicalDevice,
                                                                                     pPropertyCount: *mut uint32_t,
                                                                                     pProperties: *mut VkDisplayPlanePropertiesKHR) -> VkKhrDisplayResult;

pub type vkGetDisplayPlaneSupportedDisplaysKHRFn = unsafe extern "stdcall" fn(physicalDevice: VkPhysicalDevice,
                                                                              planeIndex: uint32_t,
                                                                              pDisplayCount: *mut uint32_t,
                                                                              pDisplays: *mut VkDisplayKHR) -> VkKhrDisplayResult;

pub type vkGetDisplayModePropertiesKHRFn = unsafe extern "stdcall" fn(physicalDevice: VkPhysicalDevice,
                                                                      display: VkDisplayKHR, 
                                                                      pPropertyCount: *mut uint32_t,
                                                                      pProperties: *mut VkDisplayModePropertiesKHR) -> VkKhrDisplayResult;

pub type vkCreateDisplayModeKHRFn = unsafe extern "stdcall" fn(physicalDevice: VkPhysicalDevice,
                                                               display: VkDisplayKHR,
                                                               pCreateInfo: *const VkDisplayModeCreateInfoKHR,
                                                               pAllocator: *const VkAllocationCallbacks,
                                                               pMode: *mut VkDisplayModeKHR) -> VkKhrDisplayResult;

pub type vkGetDisplayPlaneCapabilitiesKHRFn = unsafe extern "stdcall" fn(physicalDevice: VkPhysicalDevice,
                                                                         mode: VkDisplayModeKHR,
                                                                         planeIndex: uint32_t,
                                                                         pCapabilities: *mut VkDisplayPlaneCapabilitiesKHR) -> VkKhrDisplayResult;

pub type vkCreateDisplayPlaneSurfaceKHRFn = unsafe extern "stdcall" fn(instance: VkInstance,
                                                                       pCreateInfo: *const VkDisplaySurfaceCreateInfoKHR,
                                                                       pAllocator: *const VkAllocationCallbacks,
                                                                       pSurface: *mut VkSurfaceKHR) -> VkKhrDisplayResult;

#[cfg(windows)]
static VULKAN_LIBRARY: &'static str = "vulkan-1.dll";

#[cfg(unix)]
static VULKAN_LIBRARY: &'static str = "libvulkan-1.so";

#[derive(Default)]
pub struct VulkanKhrDisplay {
    library: Option<DynamicLibrary>,
    vkGetInstanceProcAddr: Option<vkGetInstanceProcAddrFn>,
    vkGetPhysicalDeviceDisplayPropertiesKHR: Option<vkGetPhysicalDeviceDisplayPropertiesKHRFn>,
    vkGetPhysicalDeviceDisplayPlanePropertiesKHR: Option<vkGetPhysicalDeviceDisplayPlanePropertiesKHRFn>,
    vkGetDisplayPlaneSupportedDisplaysKHR: Option<vkGetDisplayPlaneSupportedDisplaysKHRFn>,
    vkGetDisplayModePropertiesKHR: Option<vkGetDisplayModePropertiesKHRFn>,
    vkCreateDisplayModeKHR: Option<vkCreateDisplayModeKHRFn>,
    vkGetDisplayPlaneCapabilitiesKHR: Option<vkGetDisplayPlaneCapabilitiesKHRFn>,
    vkCreateDisplayPlaneSurfaceKHR: Option<vkCreateDisplayPlaneSurfaceKHRFn>
}

impl VulkanKhrDisplay {
    pub fn new() -> Result<VulkanKhrDisplay, String> {
        let mut vulkan_khr_display: VulkanKhrDisplay = Default::default();
        let library_path = Path::new(VULKAN_LIBRARY);
        vulkan_khr_display.library = match DynamicLibrary::open(Some(library_path)) {
            Err(error) => return Err(format!("Failed to load {}: {}",VULKAN_LIBRARY,error)),
            Ok(library) => Some(library),
        };
        unsafe {
            vulkan_khr_display.vkGetInstanceProcAddr = Some(std::mem::transmute(try!(vulkan_khr_display.library.as_ref().unwrap().symbol::<u8>("vkGetInstanceProcAddr"))));
        }
        Ok(vulkan_khr_display)
    }

    unsafe fn load_command(&self, instance: VkInstance, name: &str) -> Result<vkVoidFunctionFn, String> {
        let fn_ptr = (self.vkGetInstanceProcAddr.as_ref().unwrap())(instance, CString::new(name).unwrap().as_ptr());
        if fn_ptr != std::ptr::null() {
            Ok(fn_ptr)
        } else {
            Err(format!("Failed to load {}",name))
        }
    }

    pub fn load(&mut self, instance: VkInstance) -> Result<(), String> {
        unsafe {
            self.vkGetPhysicalDeviceDisplayPropertiesKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkGetPhysicalDeviceDisplayPropertiesKHR"))));
            self.vkGetPhysicalDeviceDisplayPlanePropertiesKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkGetPhysicalDeviceDisplayPlanePropertiesKHR"))));
            self.vkGetDisplayPlaneSupportedDisplaysKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkGetDisplayPlaneSupportedDisplaysKHR"))));
            self.vkGetDisplayModePropertiesKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkGetDisplayModePropertiesKHR"))));
            self.vkCreateDisplayModeKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkCreateDisplayModeKHR"))));
            self.vkGetDisplayPlaneCapabilitiesKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkGetDisplayPlaneCapabilitiesKHR"))));
            self.vkCreateDisplayPlaneSurfaceKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkCreateDisplayPlaneSurfaceKHR"))));
        }
        Ok(())
    }

    pub unsafe fn vkGetPhysicalDeviceDisplayPropertiesKHR(&self, 
                                                          physicalDevice: VkPhysicalDevice, 
                                                          pPropertyCount: *mut uint32_t,
                                                          pProperties: *mut VkDisplayPropertiesKHR) -> VkKhrDisplayResult {
        (self.vkGetPhysicalDeviceDisplayPropertiesKHR.as_ref().unwrap())(physicalDevice, pPropertyCount, pProperties)
    }

    pub unsafe fn vkGetPhysicalDeviceDisplayPlanePropertiesKHR(&self, 
                                                               physicalDevice: VkPhysicalDevice,
                                                               pPropertyCount: *mut uint32_t,
                                                               pProperties: *mut VkDisplayPlanePropertiesKHR) -> VkKhrDisplayResult {
        (self.vkGetPhysicalDeviceDisplayPlanePropertiesKHR.as_ref().unwrap())(physicalDevice, pPropertyCount, pProperties)
    }
    
    pub unsafe fn vkGetDisplayPlaneSupportedDisplaysKHR(&self, 
                                                        physicalDevice: VkPhysicalDevice,
                                                        planeIndex: uint32_t,
                                                        pDisplayCount: *mut uint32_t,
                                                        pDisplays: *mut VkDisplayKHR) -> VkKhrDisplayResult {
        (self.vkGetDisplayPlaneSupportedDisplaysKHR.as_ref().unwrap())(physicalDevice, planeIndex, pDisplayCount, pDisplays)
    }
    
    pub unsafe fn vkGetDisplayModePropertiesKHR(&self, 
                                                physicalDevice: VkPhysicalDevice,
                                                display: VkDisplayKHR, 
                                                pPropertyCount: *mut uint32_t,
                                                pProperties: *mut VkDisplayModePropertiesKHR) -> VkKhrDisplayResult {
        (self.vkGetDisplayModePropertiesKHR.as_ref().unwrap())(physicalDevice, display, pPropertyCount, pProperties)
    }
    
    pub unsafe fn vkCreateDisplayModeKHR(&self, 
                                         physicalDevice: VkPhysicalDevice,
                                         display: VkDisplayKHR,
                                         pCreateInfo: *const VkDisplayModeCreateInfoKHR,
                                         pAllocator: *const VkAllocationCallbacks,
                                         pMode: *mut VkDisplayModeKHR) -> VkKhrDisplayResult {
        (self.vkCreateDisplayModeKHR.as_ref().unwrap())(physicalDevice, display, pCreateInfo, pAllocator, pMode)
    }
    
    pub unsafe fn vkGetDisplayPlaneCapabilitiesKHR(&self, 
                                                   physicalDevice: VkPhysicalDevice,
                                                   mode: VkDisplayModeKHR,
                                                   planeIndex: uint32_t,
                                                   pCapabilities: *mut VkDisplayPlaneCapabilitiesKHR) -> VkKhrDisplayResult {
        (self.vkGetDisplayPlaneCapabilitiesKHR.as_ref().unwrap())(physicalDevice, mode, planeIndex, pCapabilities)
    }
    
    pub unsafe fn vkCreateDisplayPlaneSurfaceKHR(&self, 
                                                 instance: VkInstance,
                                                 pCreateInfo: *const VkDisplaySurfaceCreateInfoKHR,
                                                 pAllocator: *const VkAllocationCallbacks,
                                                 pSurface: *mut VkSurfaceKHR) -> VkKhrDisplayResult {
        (self.vkCreateDisplayPlaneSurfaceKHR.as_ref().unwrap())(instance, pCreateInfo, pAllocator, pSurface)
    }
}
